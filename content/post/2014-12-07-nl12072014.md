---
date: 2014-12-07
date: 2014-12-07
layout: post
title: Weekly Newsletter (12-07-2014)
category: posts
draft: false
---
This week, I'm experimenting with something new.
I've annotated certain articles I read and shared links
to them, in addition to their original versions.
Please let me know what you think of this. I don't want to
share the annotations if you aren't interested in seeing them. On the
other hand, if you like the annotated articles enough, I may start
only sending annotated versions of certain articles. Finally, if you are
someone who contributes articles (or wish to) and you want to annotate your
articles too, I can help you get up and annotating within 10 minutes.

# Critical Thinking
[A Weapon for Readers (Normal)](http://www.nybooks.com/blogs/nyrblog/2014/dec/03/weapon-for-readers/)
**[(Annotated)](https://diigo.com/074oyv)**  
Asserts that the act of marking up something while you read it provides
myriad benefits. While reading this, I was struck by the
fact that I don't mark up any of the articles I read. Thus, this article
compelled me to annotate certain articles this week and share my
annotations with you.

[The Mystery of Go, the Ancient Game That Computers Still Can't Win
(Normal)](http://www.wired.com/2014/05/the-world-of-computer-go/#)  
Discusses why computer Go programs still cannot beat most professional
Go players. I enjoyed this article except for the parts where the author
goes on tirades against AI optimists and futurists. Unfortunately, I
couldn't get annotations to work on this article, but suffice it to say
there were a lot of places where the author injects
his own religious and philosophical views implicitly. So be wary of
trusting all of his opinions about the larger implications of the story.
# Foreign Policy
[Losing the War (Normal)](http://www.leesandlin.com/articles/LosingTheWar.htm)  
I feel obligated to start off by warning you that this piece is LONG. I
would guess that a page count of it would come in at around 100 pages.
That being said, I included it in the newsletter and chose to include
fewer other articles as a result. If even one reader of the newsletter reads this, I
will be happy. The content of this piece spans almost all of World War
II. But, at its core, the narrative attempts to distill what it was
actually like to be in the theater of war and how are memories of the
war fade and change. So, if you do end up reading this piece, please let me
know, for my sake. I'd love to talk with you about it.

[What the Media Gets Wrong About Israel (Normal)](http://www.theatlantic.com/international/archive/2014/11/how-the-media-makes-the-israel-story/383262/)  
Argues that the media, in particular the Associated Press (for which the
author of the piece previously wrote) possesses a bias against Israel.
First, my opinion on this issue is biased by a conflict of interest. I
am an unashamed supporter of Israel. That being said, as a supporter of
Israel, I have watched the media's coverage of ongoings in the country
and around it. I firmly believe that this article is totally accurate in
its portrayal of the bias against the Israeli state. In particular, the
Goldstone report, alluded to in this article, presents a prime example
of the way in which NGOs and the media collaborate to create a false
narrative surrounding Israeli-Palestinian relations. I'd love to talk
more about this article. Please don't hesitate to
contact me to talk about it.

[The Return of Africa's Strongmen (Normal)](http://www.wsj.com/articles/the-return-of-africas-strongmen-1417798993) **[(Annotated)](https://diigo.com/074ovt)**  
*Note: This was contributed by Matt Ritter, but I chose to include it in this
since it is about Foreign Policy.*
Discusses how many African countries have failed to move from autocratic
regimes to democratic governments.

# Other Contributions
[Free Speech is so last century. Today's students want the 'right to be comfortable'](http://www.spectator.co.uk/features/9376232/free-speech-is-so-last-century-todays-students-want-the-right-to-be-comfortable/) (Contributed by Kevin, described by a quote)  
> if we don’t allow our opinion to be ‘fully, frequently, and fearlessly discussed’, then that opinion will be ‘held as a dead dogma, not a living truth’ - John Stuart Mill

---
date: 2015-04-05
date: 2015-04-05
layout: post
title: Weekly Newsletter
category: posts
draft: false
---
# Mind and Brain
[Electrified](http://www.newyorker.com/magazine/2015/04/06/electrified)  
Investigates the promises and perils of tDCS, the electrical stimulation of the brain proponents claim will help treat depression and other neurological disorders.

# Evolution
[Defending Darwin](https://orionmagazine.org/article/defending-darwin/?src=longreads)  
A biology professor at the University of Kentucky discusses his experiencing teaching evolution to a group largely composed of Creationists. This article interests me on two levels: 1) I come from an area where evolution is largely unchallenged. In school, I don't ever recall one of my peers arguing with the teacher about evolution, so it's interesting to read about a place where this is not the case. 2) This teacher manages to change students minds (even if it's only a few) on beliefs they hold strongly and have been brought up with.

# Iran
[Iran Deal Threatens to Upend a Delicate Balance of Power in the Middle East](http://foreignpolicy.com/2015/04/02/iran-deal-threatens-to-upend-a-delicate-balance-of-power-in-the-middle-east-saudi-arabia-nuclear-deal/)  
Opposes the Iranian nuclear deal on the grounds that it sends negative signals to other American allies in the region.

[I'm a Republican and I Support the Iran Nuclear Deal](http://foreignpolicy.com/2015/04/02/im-a-republican-and-i-support-the-iran-nuclear-deal/)  
Supports the Iranian nuclear deal. I found reasons 1, 3, and 4 compelling and found the other two less convincing. Of the articles in support of the deal, this did the best job of succinctly summarizing the case for the deal.

# Agriculture and Green Design
[Grown in Detroit but not in the ground](http://www.modeldmedia.com/features/indoorurbanagriculture052014.aspx)  
Profiles two hydroponics projects in Detroit. It's interesting to see how the two projects differ despite their similar goals.

[This designer doesn't make chairs. He grows them--from trees](http://qz.com/376807/growing-chairs/)  
Profiles a design firm that grows furniture using a special technique they've devised.

# Free Speech
[Mute Button](http://www.newyorker.com/magazine/2015/04/13/mute-button)  
Laments the worldwide devaluing of free speech. The author's totally on point about the more subtle dangers of self-censorship.

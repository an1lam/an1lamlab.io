---
date: 2018-07-01
date: 2018-07-01
layout: post
title: Fact Post - How Safe Is Biking in NYC?
category: posts
draft: true
---
I want to practice changing my mind from data. Biking is a good testing ground because I've only recently started doing it and I'm not emotionally attached to it. 

## Personal Pre-Registration
Before I dive into the data, I should decide what it would take for me to decide to stop biking to and from work 2 to 3 times a week, as I've recently started doing.  This is hard because my risk tolerance in % terms isn't very calibrated. That said, I do know that there are certain forms of transportation that I currently feel comfortable using as ways to get to work (and around the city in general): walking, riding the subway, and riding in cars. Given that I'm implicitly treating all three of these as acceptable risks, calibrating my willingness to bike around how much more or less risky biking is in comparison to them seems like a good starting point. I'll use risk of injury/fatality per mile because it fits my personal travel pattern (I have to go the same distance each day regardless of how I do it) and matches the risk measure used in a [Comparison of 2013 VMT Fatality Rates in U.S. States and in High-Income Countries][3] study I found. In concrete terms, I will feel comfortable continuing to bike to work if, on a per-mile basis, biking isn't more than 2x as dangerous than the more dangerous of walking or riding in a car. 

While you'll have to trust me when I promise that I wrote this pre-registration before doing the research, I also don't have a strong incentive to lie.

## Plumbing the Data
Turns out there's an abundance of data about the number of bike, pedestrian, and car crashes in New York County (Manhattan) and scarce information about the number of miles people travel via each mode of transportation.

- According to [NYC Green Dividend][1], the average New York City resident drives 9 miles per day.
- Ideally, I want a crashes-per-mile in New York County number for the past 10 years.
- Questions to answer:
- Is biking safer than driving?
- Is biking safer than walking?
- Are there any strategies for making biking safer as an individual?
- Is biking significantly safer on certain roads?

## References
[1]: http://www.nyc.gov/html/dot/downloads/pdf/nyc_greendividend_april2010.pdf "NYC Green Dividend"
[2]: https://www.nymtc.org/LinkClick.aspx?fileticket=0HLTJMvnqN0%3D&portalid=0 "NYC Area 2017 Vehicle Miles Traveled by County"
[3]: https://crashstats.nhtsa.dot.gov/Api/Public/Publication/812340 "Comparison of 2013 VMT Fatality Rates in U.S. States and in High-Income Countries"

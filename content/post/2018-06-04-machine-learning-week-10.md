---
date: 2018-06-04
date: 2018-06-04
layout: post
title: Coursera ML
subtitle: Week 10 - Large Scale Machine Learning
category: posts
draft: true
mathjax: true
---
# Large Scale Machine Learning
## Gradient Descent With Large Datasets
> It's not who has the best algorithm that wins. It's who has the most data.
- We want to be able to learn with large datasets. This rings very true with my experience. Data scientists at Compass were handicapped by the lack of large datasets.

- Take linear regression as an example.
- How do we sanity check if a smaller training set will do just as well? If we have high variance with a small training set, then it's worth trying our algorithm on a bigger dataset. High variance often means there's too little structure so we're capturing surface-level features.

### Stochastic Gradient Descent
> The danger of the race to the bottom is that you race to the wrong bottom.

#### My Pre-Watch Prediction
For stochastic gradient descent, the goal is to avoid local minima. We therefore need a way to jump from one hill in our cost manifold to another. I can imagine doing this by occasionally randomly moving $$\theta_j$$ while we're descending.

#### Review of Batch Gradient Descent
Batch gradient descent is O(m) for each gradient adjustment. That's really slow.

#### Stochastic Gradient Descent
$$
cost(\theta, (x^{(i)}, y^{(i)})) = \dfrac{1}{2}(h_{theta}(x^{(i)}) - y^{(i)})^2
$$

With stochastic gradient descent, we randomly shuffle the dataset and then, incrementally update $$ \theta_j $$ by taking a little bit off with each example, as opposed to averaging the update from all $$x$$s.

We're trading wandering around our region more for better performance.

*Question*: Does this do anything to avoid local minima or is that totally unrelated to this?

For posterity, the stochastic gradient descent algorithm is:


    repeat {
      for i := 1,...,m {
      	update gradient
      }
    }

where "update gradient" means:

$$ 
\theta_j := \theta_j - \alpha(h_{\theta}(x^{(i)}) - y^{(i)})x_{j}^{(i)}
$$

### Mini-Batch Gradient Descent
Mini-batch gradient descent is a compromise between full-on batch and stochastic gradient descent. For each outer loop, we use a small number, $$b$$, examples to update $$\theta_j$$.

Mini-batch gradient descent maximizes the benefits of parallelization of vectorized operations by optimizing batch sizes to match parallel limits of processors.

### Stochastic Gradient Descent Convergence
> So that he who wanders does not become lost.

For normal batch gradient descent, we'd plot $$J_{train}(\theta)$$ as a function of the number of iterations of gradient descent. This was too slow to do in realtime however, because computing $$J_{train}$$ required computing the sum of the output of a function of all of our $$m$$ inputs.

For stochastic gradient descent, computing $$cost(\theta, (x^{(i)}, y^{(i)}))$$ is cheap enough that we can compute it as we're updating $$\theta$$.

Here's what I don't understand. He's showing all these plots, but I don't see how we can comfortably monitor $$J_{train}$$ as we adjust $$\theta$$ in real time because we don't have a way to know what granularity of trend we can reliably act upon.

If we're worried about convergence, we can slowly decrease $$\alpha$$ over time. For example, $$ \alpha = \dfrac{const1}{iterationNumber + const2} $$. Obviously, we have to finick with the constants.

## Advanced Topics
### Online Learning
Oh yeah, this is my stuff! I've been curiuos about this for so long. How do we update our algorithm's features using data streaming in from our product's activity?

#### Shipping Service Example
An example of an online learning problem: shipping service website where user gets a quote for shipping an item and does or doesn't decide to ship their item using our service.

Features: $$x$$ captures the properties of user, of origin/destination, and of asking price. We want to learn $$p(y = 1\|x;\theta)$$ to optimize price.

To model this, we can start by using logistic regression.

Assume our website data capture and online learning algorithm is:

```
repeat forever {
  get (x, y) corresponding to user
  update theta using (x, y)
}
```
where "update theta" means:
$$
\theta_j := \theta_j - \alpha(h_{theta}(x) - y)x_j
$$

Notice we don't have $$x^{(i)}$$ because we're updating our gradient one item at a time.

This is surprisingly simple!

An interesting property of this online learning algorithm is that it can adapt to changing user preferences.

#### Product Search Example
We're selling cell phones. Users come to our site and search for them. Our search returns 10 results out of the 100 we have in store.

Features: 
* $$x = $$ features of phone, how many words in user query match name of phone, how many words in query match description of phone, etc.
* $$y = 1$$ if user clicks on link. $$y = 0$$ otherwise.
* Learn $$p(y = 1\|x;\theta)$$. This is known as the problem of learning the predicted click-through rate (CTR). We can use this to show the user the 10 phones they're most likely to click on.

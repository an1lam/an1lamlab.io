---
date: 2018-01-25
date: 2018-01-25
layout: post
title: Marginalia - Gödel, Escher, Bach
category: posts
draft: true
---
How much do isomorphisms play a role in our understanding of abstract concepts?
> Sometimes it is helpful to think of the LISP statements as mere pieces of data which are fed
> sequentially to a constant running machine language program (the LISP interpreter). When you
> think of things this way, you get a different image of the relation between a program written in
> a higher-level language and the machine which is executing it.
- Hofstadter's other essay about analogy as the core of cognition.
- Hofstadter's a modern sort of Platonist (need quote from Bongard problems chapter).
- Include something about CJS Hayward's post about an abstract art of memory. Mention a disclaimer
  about Hayward's other posts.
- I wish I could talk to Hofstadter and ask him to what degree the isomorphisms and intuition
  proceeded the formalisms for him.
- Example of DNA going through the tape recorder.
> Let us first give a picturesque image, and then render it more precise. Imagine the mRNA to be
> like a long piece of magnetic recording tape, and the ribosome to be like a tape recorder. As the
> tape passes through the playing head of the recorder, it is "read" and converted into music, or
> other sounds. Thus magnetic markings are "translated" into notes. Similarly, when a "tape" of
> mRNA passes through the "playing head" of a ribosome, the "notes" which are produced are *amino
> acids*, and the "pieces of music" which they make up are *proteins*. (GEB, 519)
- Take this intriguing passage from an interview of Hofstadter as well.
> Hofstadter had always been fascinated with how the mind works. He set out to teach himself
> Euclidean geometry as an experiment, a way of observing the process of intellectual reasoning and
> discovery. He used the humble triangle as his guinea pig. “I was so naive at the outset of my
> many-year binge with geometry that I was caught off guard by the fact that the triangle had more
> than one centre,” says Hofstatder. “I had heard the terms ‘orthocentre,’ or ‘centroid,’ and
> ‘circumcentre.’ I sort of knew these things existed, but to tell the truth, when I found out that
> any triangle has several centres, I was really thrown. It seemed like a miracle. In point of
> fact, any triangle has an infinite number of centres, but they are not all equally interesting.”
>
> “One time,” Hofstadter continues, “I made an analogy to the human body.  Suppose I were to ask
> various people, ‘What is the most important part of your body?’ One person might reply, ‘It’s my
> brain.’ Somebody else might say, ‘No, it’s my stomach;’ somebody else might insist ‘my heart,’
> ‘my sexual organs,’ or ‘my belly button’....I became inflamed and impassioned in trying to invent
> new geometrical notions. And I saw that the process of analogy-making lay at the core of all my
> discoveries. I started noticing beautiful patterns not only in triangles and among triangle
> centres, but there were exquisite patterns at the level of the ideas themselves.”
>
> Following his triangle experiment, Hofstadter returned to math with the intention of reclaiming
> it. “I came back to group theory and Galois theory some thirty-five years later and I said, ‘By
> god, I’m going to make this hateful stuff lovable.’  ” He started a course called Group Theory
> and Galois Theory Visualized, though the running joke is that his approach is more like Pizza
> Theory.  The visual metaphor Hofstadter uses to explain difficult group theory theorems such as
> Zassenhaus’ lemma is simple circles with slices and concentric circles within, which look a lot
> like pizzas. (It didn’t hurt either that he always brought pizzas to class.) Hofstadter’s
> overriding conclusion from these intellectual experiments is that analogy-making plays a crucial
> role in doing quality mathematics—analogies are what allow the illustration and conceptualization
> and understanding of otherwise seemingly unfathomable ideas.

Is intelligence skimmable? Was the GOFAI goal to "explain the high-level traffic of symbol
activation in its own terms (GEB, 358)" misguided?

Is Hofstadter's comment about visualization arising as a result of simulated motor activity at all
proven?
> What magic allows us to mesh two or three images, hardly giving a thought as to how we should do
> it? Knowledge of how to do this is among the most procedural of all, for we have almost no
> insight into what mental imagery is.
>
> It may be that imagery is based on our ability to suppress motor activity. By this, I mean the
> following. If you imagine an orange, htere may occur in your cortex a set of commands to pick it
> up, to smell it, to inspect it, and so on. Clearly these commands cannot be carried out, because
> the orange is not there. But they can be sent along the usual channels towards the cerebellum or
> other suborgans of the brain, until, at some critical point, a "mental faucet" is closed,
> preventing them from actually being carried out. Depending on how far down the line this "faucet"
> is situated, the images may be more or less vivid and real-seeming.

Does the fact that our brains are plastic change disprove or call into question Hofstadter's point
about the separation between software and hardware in brains?
> We humans also have "software" and "hardware" aspects, and the difference is second nature to us.
> We are used to the rigidity of our physiology: the fact that we cannot, at will, cure ourselves
> of diseases, or grow hair of any color--to mention just a couple of simple examples. We can,
> however, "reprogram" our minds so that we operate in new conceptual frameworks. The amazing
> flexibility of our minds seems nearly irreconcilable with the notion that our brains must be made
> out of fixed-rule hardware, which cannot be reprogrammed.
- Argument for: major improvements to thinking may correlate with physiological changes.
- Argument against: brain and perception shifts happen much too rapidly to require physiological
  change. Physiological change more likely correlates with lower level capabilities and solidified
  memory.

Have people solved the bootstrapping problem in biology?

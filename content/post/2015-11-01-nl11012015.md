---
date: 2015-11-01
date: 2015-11-01
layout: post
title: Weekly Newsletter
category: posts
draft: false 
---
## Journalism
[The Future of News is not an
Article](http://nytlabs.com/blog/2015/10/20/particles/)  
A NY Times Labs writer argues that the future of news lies in smaller,
contextually-aware primitives called particles. If you've never explored the NYT
Labs website before, I highly recommend it. I particularly enjoy
[this](http://nytlabs.com/projects/chronicle.html) infographic.

## Biography
[Genghis
John](https://dl.dropboxusercontent.com/u/52781209/Publications/Genghis%20John.pdf)  
Paints a portrait of John Boyd, military theorist and legendary aircraft
designer. Boyd's devotion to his ideals and unconventional thinking resonates
with me and I definitely intend to read his papers and books. I expect the more
I learn about Boyd's ideas the more I will be able to use his framework for
thinking for my own purposes.

## Business
[Semco--Insanity that
Works](http://www.freibergs.com/resources/articles/leadership/semco-insanity-that-works/)  
Profiles Semco, an unconventional Brazilian business. Semco eschews all
hierarchy and corporate bureacracy. Semco provides an unprecedented level of
autonomy to its employees and manages to make a consistent profit while doing
so. The author of this article wears his bias towards the company on his
shoulder, but the company itself sounds interesting enough that I'm willing to
overlook this issue.

## Cryonics
[The Scientific Basis of Cryonics](http://bit.ly/1MaGycQ)
A spiritual successor to the article I included a few weeks back about the girl
who was cryonically frozen. This piece focuses on the scientific feasibility of
cryonics. The authors argue that cryonics presents a viable option for preserving brain
information, even though we don't know the mechanism by which that information
is encoded.

## Government
[Design of a Digital Republic](http://bit.ly/1KREfWL)
Discusses the components of a well-functioning network from a political
perspective. This is the first in a series of articles written by the creators
of [Urbit](urbit.org). Urbit's fallen under controversy due to one of its
founders previous heavy involvement in the Neoreactionary movement. While I'm
unsympathetic to Neoreactionism, I'm very excited about Urbit and share their
views about the characteristics of the ideal future network.

## Other Contributions
### David Wylie
[The Transformation of David
Brooks](http://www.cjr.org/the_profile/the_transformation_of_david_brooks.php)  
Bringing together social science, politics, and morality is something David
Brooks has traditionally done and still does. However, this piece gives an
interesting account of his shift in focus. Brooks evolved from a political
pundit, to a voice for what I think could be labeled as 21st century social
conservatism - this piece does a good job of providing insight into his
development as a controversial countercurrent to the "relativistic strain" of
the world. (That being said, I do highly recommend Brooks' The Social Animal,
which is much heavier on synthesizing social science research, though the moral
undertones are there).

"In general, Brooks contends, journalists balk at sharing moral viewpoints, and
readers bristle upon receiving them. His critics find him an insufferable scold,
a pompous sermonizer. 'I think there is some allergy our culture has toward
moral judgment of any kind,' he reflects. 'There is a big relativistic strain
through our society that if it feels good for you, then who am I to judge? I
think that is fundamentally wrong, and I’d rather take the hits for being a
moralizer than to have a public square where there’s no moral thought going on.'
There is at least marginal evidence that this is changing. His book, published
in April, spent 22 weeks on the Times best-seller list."

[Our first-ever college rankings](http://www.economist.com/blogs/graphicdetail/2015/10/value-university)
College rankings, in general, suck. I'm not sure The Economist's first ever
college rankings tell us much more, but they at least attempt to put a thorough
methodology behind value-added thinking. I can't help but think that college
rankings will be continually plagued by the problem of defining value - is it
monetary? social? intellectual? And even if we take a broader view of value, how
do we assess it? What are the proxies? Or are we overcomplicating this - is
income a close enough of a proxy for the dynamic effect of the many criteria?
I'm inclined to believe no, but as the cost of college becomes something
Americans are increasingly paying attention to, the focus on monetary benefits
will be intensified. Ultimately, I don't think that's a bad thing - rankings
just need to be clearer about their objectives. Credit to The Economist for
doing so and recognizing the limitations of their rankings:

"Finally, maximising alumni earnings is not the only goal of a college, and
probably not even the primary one. In fact, you could easily argue that
“underperforming” universities like Yale and Swarthmore are actually making a
far greater contribution to American society than overperformers like Washington
& Lee, if they tend to channel their supremely talented graduates towards public
service rather than Wall Street. For students who want to know which colleges
are likely to boost their future salaries by the greatest amount, given their
qualifications and preferences regarding career and location, we hope these
rankings prove helpful. They should not be used for any other purpose."

### Will Baird
[Red Meat Is Not the
Enemy](http://www.nytimes.com/2015/03/31/upshot/red-meat-is-not-the-enemy.html?_r=0)
Given the WHO's recent announcement about red meat, this article from March is
relevant.


---
date: 2015-04-29
date: 2015-04-29
layout: post
title: My Meditation Practice - Part I
category: posts
draft: true
---
## Table of Contents
1. [Background](#background)
2. [Current Practice](#current-practice)
3. [Struggles and Open Questions](#struggles-and-open-questions)



## Background <a id="background"></a>
As most people who know me in real life know, I'm a regular meditator and spend way too much time thinking about meditation-like practices. I've played around with various techniques over the last few years, but have mainly focused on mindfulness of breathing. I recently started to grow disillusioned with my practice, however. In this post, I'll hopefully elucidate my reasons for becoming disillusioned with my previous Buddhist-inspired practice, explain my new practice, and give some insight into the obstacles I currently face.

My goals do not line up particularly well with those of the Buddhist community. For one, I'm not the least bit interested in renunciation as they define it. Theravada Buddhists follow a set of precepts known as the *[vinaya]*, a list of 227 Buddhist "no-nos" that includes precepts such as (I'm paraphrasing) "thou shalt not take intoxicants" and "thou shalt not kill", etc. Many American meditation practitioners don't abide by these precepts, but they do form the foundation of Buddhism in the same way that the 10 Commandments form the foundation of Judaism. Thus, I suspect that most serious lay Buddhist practitioners at least wish they could follow these precepts, whereas I do not. Many leading Buddhist thinkers also encourage retreats and even monastic life as a prerequisite for practitioners to progress. While there's nothing inherently wrong with a monastic life or going on frequent long Buddhist retreats, they're just not my cup of tea. Finally, the elephant in the room is the Buddhist doctrine surrounding the self. The more I learn about this doctrine, the more I find it confusing, so I won't even try to describe it here. Basically, Buddhist views on the self don't agree with me. This may mean I'm intentionally ignoring the reality and doomed to suffering, but so be it. I don't care. I'm not interested in discovering my no-self or neither-no-self-or-self nature. As Case says in *[Neuromancer]*, it's just not my trip.

This brings up the question of why I even practice meditation in the first place. It's a practical thing. The mind is the root of experience. Everything stems from it. Meditation is mind training. Ever since I saw Star Wars when I was young, I've had this intuition that the mind is powerful (not necessarily in a supernatural sense, just in the sense that it's the filter for every experience). My belief in the mind's principal importance and its malleability has generated a desire for more power over my mind. Initially, my attempts at mind enhancement focused on Buddhist and modern mindfulness-derived techniques, but while I still employ and take heed from aspects of these traditions, I've become more interested in more imagination-focused techniques. In addition to the reasons discussed above, I've moved away from Buddhist-inspired techniques as they tend to label imaginative thinking as *[papanca]*.

To summarize, I've become disillusioned with Buddhist techniques because they lead to an eventual scenario where I might have to give up my attachments and self-aggrandizement, which I'm altogether unwilling to do.

### Current Practice <a id="current-practice"></a>
My current interests revolve around visualization and mind palaces. One thing I gained from my time studying and practicing Buddhist-inspired techniques is a firm understanding of the power of absorption through concentration. While it's referred to by different names in different traditions, the idea of intense focus through absorption in a single object can be found in almost every religious tradition (in Buddhism - [jhana], in Hinduism - [dhyana] and [samadhi], in Christianity - mystical practices performed by [Teresa of Avila]).  I'm particularly curious about what happens when you combine absorption / jhana / dhyana (very similar things) with a visualization of your mind palace.

Lately, my meditation's been focused on a technique I found in a book about [Jewish Meditation] by Aryeh Kaplan where you try to visualize a letter in your mind's eye. It's been slow going, but based on what I've read, eventually a process called 'engraving' will occur where the letter becomes fixed in your head. It seems like this is the first step towards better understanding the type of vivid visualizations that I've read about in some older texts. Furthermore, the 'engraving' process sounds excitingly similar to descriptions of absorption on the traditions discussed above. Getting to 'engraving' therefore seems like a great first step for my mission of combining these two powerful techniques. I don't want to speculate beyond this because my experience with meditation in general has lead me to the conclusion that experience trumps knowledge. Descriptions of states experienced in deep meditation often make sense after experiencing the state for yourself. So, I'm hoping it becomes more clear how to transfer this letter visualization to more complex visualizations and daily life once I better understand the fundamentals of the technique.

Simultaneously, I've experimented with mind palaces, reading Ed Cooke's book [Remember, Remember: Learn the Stuff You Thought You Never Could], and have mind palaces for the US Presidents, British Prime ministers, and the countries of Europe. However, I find that these mind palaces are more like still pictures that flash in my mind rather than full-on 3D virtual realities that I can move through with details. Moving from the former to the latter is my ultimate goal.

## Struggles and Open Questions in my Visualization Practice <a id="struggles-and-open-questions"></a>
### Contortion of the Visualized Object
The major problem I experience in my practice of visualization is losing the image I'm supposed to be focusing on. While this in itself is presumably an expected component of the training process, the part I struggle with is the mechanics of the image's fading. When visualizing an 'A', the lines in the 'A' don't just disappear but contort into curves or twist such that the angles of the character no longer make sense.

So far, I've found that the best way to deal with this is to let go of these lines and reinstantiate the 'A' in its original form. Trying to force the lines back into position always fails.

### Fading of the Visualized Object
The other problem I face related to losing the visualization is distraction. I divide distractions into two types thought-or-sensation-based and image-based. Thoughts or sensations distract me from visualization in the same way they distract any meditator. Dealing with these involves gently directing my mind back to the object.

Image distractions arise when that emerge from the blackness of my eyelids obscure the image I'm visualizing. This type of distraction is more troubling. I struggle to deal with these images because the mechanics of visualization are unclear to me. I go back and forth on whether my visualizations exist independent of my actual eyes or occur as an overlay on top of the blackness seen when my eyes are closed. My intuition is that the former better describes the mechanism through which visualization actually operates. However, certain descriptions in texts I've browsed suggest the latter. For now, I'm going to assume the former but would appreciate the help of a more experienced practitioner in navigating this territory.


### Visualized Object doesn't manifest clearly
The final obstacle I routinely encounter in my letter visualization practice is an inability to clearly manifest the image of the letter in my mind's eye. While some traditions recommend using the retinal afterimage of the object in real life, this technique would not lead to my goal of accurately visualizing objects of significant complexity whenever I please. As a result, when visualizing, I often feel like whatever image I'm visualizing in my mind's eye is too blurry to ever turn into an accurate version of the object in real life. Some texts state that vividness increases with practice and absorption, so it seems like my best bet is to keep practicing and hope, but some outside assurance that this is the case would make me feel much better.

[jhana]: http://www.accesstoinsight.org/ptf/dhamma/sacca/sacca4/samma-samadhi/jhana.html
[vinaya]: http://www.accesstoinsight.org/tipitaka/vin/
[papanca]: http://en.wikipedia.org/wiki/Conceptual_proliferation
[samadhi]: http://en.wikipedia.org/wiki/Samadhi
[Teresa of Avila]: http://en.wikipedia.org/wiki/Teresa_of_%C3%81vila
[Jewish Meditation]: http://www.amazon.com/gp/product/0805210377/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=0805210377&linkCode=as2&tag=stepmali-20&linkId=NPV2IKXWTAIDI2AW
[Remember, Remember: Learn the Stuff You Thought You Never Could]: http://www.amazon.com/gp/product/B002RI9GUI/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B002RI9GUI&linkCode=as2&tag=stepmali-20&linkId=643WF56NLAGFGZYT
[Neuromancer]: http://www.amazon.com/gp/product/B000O76ON6/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B000O76ON6&linkCode=as2&tag=stepmali-20&linkId=CMXVA4VK6K4UULII

---
date: 2016-03-06
date: 2016-03-06
layout: post
title: Stoicism 2.0
draft: true
category: posts
---
## My History with Stoicism
I can't remember when I first stumbled upon Stoicism. I suspect Tim Ferriss and
Ryan Holiday led me to further investigate Stoicism.


I think of Stoicism as a framework for thinking. I apply Stoic ideals to
decisions on a daily basis.
* Tenets of Stoicism 1.0
	* Virtues as ends in themselves
	* Honesty
	* Reason: control (subjugation?) of Emotions / Affectivity
		* A priori assumption that everything that happens is exercising
		  its Nature
	* Independence from external influences
		* Intermittently practicing & regularly imagining hardship
		* Contemplative journaling 
		* Focused re-reading and contemplation of important works
	

* Why not just create something new?
	* Humility - not smart enough to come up with my own philosophy
	* Sense of being part of a lineage / tradition under-valued in our society
	* Respect for Seneca, Aurelius
* Why talk about Stoicism in the first place?
	* Stoicism is hot
	* "Householder tradition"
	* Fits mostly with modern sensiblities
	* Light on metaphysics, heavy on practical strategies
	* Multiple viewpoints existed historically so Stoically-inclined people not
	  prone to "one-true-guru" fallacy

Why remove pessimism from Stoicism?
* Belief in real possiblity for lasting change / to be around to see the future
	* What's different now?
		1. Can't convince anyone if they don't already believe this
		2. Can provide technological predictions / trends
			* Is it worth it?
* Thinking negatively affects the subconscious
	* If you assume things will turn out badly, they probably will
	* Accept that thinking positively may not change anything but thinking
	  negatively probably will



Optimism as a virus that infects culture. Optimism about the material world as a
recent phenomenon. Need historical evidence for this. Is it really true that
optimism was much less common in the past (seems dubious)? Does it matter?
Perhaps this is an unimportant point.

How to approach the optimism point?
* Talk about basic tenets of Stoicism and provide external references for more
  information
	* Universal Virtues and living based on Nature
		* Stoic conception of nature different from naturalism
	* Growing stronger from hardship
		* Reference gettingstronger.org
	* Transience
			

How does replacing pessimism with optimism improve Stoicism?
* More palatable to modern sensibilities
* Further incentivizes Stoics to act in the world
* Eliminates the notion that all change is transient
* Synergistic with transhuman ideals

Why does determinism not make sense as a philosophy even if its correct?
* Thinking deterministically causes apathy
* Ted Chiang story with the button

Questions:
Is there already a "Stoicism 2.0" movement?
Is there a better name for what I'm talking about?
Should I break the discussion up into smaller components?


---
date: 2014-11-29
date: 2014-11-29
layout: post
title: Book Review - Gentle Bridges, Conversations with the Dalai Lama on the Science of Mind by Jeremy W. Hayward
category: posts
draft: true
---
Reading [Gentle Bridges](http://www.amazon.com/Gentle-Bridges-Conversations-Dalai-Sciences/dp/1570628939), I found myself wishing that the Dalai Lama would talk more and the scientists would talk less. The author structures the book so that most of the book is spent on lectures given by scientists visting the Dalai Lama about the foundational methodologies and findings of their respective field. He begins with an explanation of the scientific method and then transcribes the discussions of neuroscience, cognitive psychology, artificial intelligence, and evolution. A discussion then follows each lecture. During the lectures, the Dalai Lama interjects infrequently. Although he talks more during the discussions, he still makes up only a minority of the conversation. This is unfortunate because the Dalai Lama's words are filled with insight. The following quote serves as a good example of one of his many aphorisms. While discussing scientists' conditioning in their disciplines, he opines\:  

>"So the conditioning can go both ways. It can lead you deeper into reality or it can lead you right away from reality and actually distort your mode of experience."

All this is not to say that I didn't enjoy the scientists' overviews of their fields. As one might expect, the scientists who visited the Dalai Lama appear to be some of the more creative members of their fields. As a result, they were both more open to the Dalai Lama's ideas and brought more unconventional ideas to the table than the average scientist.

Part of the goal of this meeting of the minds was for the scientists to educate the Dalai Lama on the foundations of a few of our scientific disciplines, beginning with. In this sense, the meeting was a success. The Dalai Lama clearly enjoyed and learned from the exchange.

On the whole, *Gentle Bridges* piqued my interest in Tibetan Buddhism and the Dalai Lama, but failed to provide me with enough knowledge on the topics to feel fulfilled.
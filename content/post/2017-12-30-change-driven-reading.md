---
date: 2017-12-30
date: 2017-12-30
layout: post
title: Change-Driven Code Reading
category: posts
draft: true
---
That none of us read enough code is one of the few things on which programmers agree. Taken
together, the interviews in Peter Seibel's book, [Coders At Work](http://amzn.to/2zjbCUE) and on his
[blog](http://www.gigamonkeys.com/code-quarterly/2011/hal-abelson/), highlight a comical
contradiction: almost all the programmers interviewed by Seibel recommend that others read code for fun,
but none of them routinely does so themselves. After noticing this, Seibel even asked Hal Abelson
(of SICP fame) about this phenomenon directly in an interview:
> Seibel: I want to dig a little deeper on this. You, like many other people, say programmers should
> read code. Yet when I ask what code have you read for fun or edification, you—also like many other
> people—answer that you read students’ code, which is your job, and review code at Google, which is
> also your job. But it doesn’t sound like you sit down of an evening with a nice printout and read
> it.

[Seibel](http://www.gigamonkeys.com/code-reading/), [James
Hague](http://prog21.dadgum.com/194.html), and [others](http://disq.us/p/qvter6) have all tried to
justify why code reading is so uncommon (see
[here](https://lobste.rs/s/p1ycfg/code_is_not_literature_2014#c_xgiqc1) for Kartik's argument
against Seibel's). While I agree with some of the ideas expressed in each of these arguments,
the authors all seem to ignore one key fact: we ("we" generally refers to "programmers" throughout
this essay) all read code already! We read code whenever we want to change it. And while
change-driven reading may not always be pleasant or effective, it works well
enough that our massive software systems haven't yet collapsed ([oh, but they will](https://www.theatlantic.com/technology/archive/2017/09/saving-the-world-from-code/540393/);
[no, they won't](https://www.tbray.org/ongoing/When/201x/2017/11/27/Software-Reality)).

Kartik and I recently discussed the importance of change-driven reading in the context of getting
people to read [Mu](https://github.com/akkartik/mu) programs. Kartik pointed out that, in spite of
all the nice Mu features he's built, readers understand Mu code best when they keep in mind a change
they want to make to the codebase while they read. In his words:
> I knew that you needed to read Mu programs in your programming environment of choice rather than a
> browser window. But I'm starting to realize that it's not enough to just be bouncing around inside
> Vim or whatever. If you don't have a concrete desire you want to see manifested in the codebase,
> you probably aren't motivated to run the program, run at individual layers, compare the tangled
> output at different layers to see how they fit together, etc.

In thinking about change-driven reading on my own after our conversation, I concluded that I'd
never achieved a satisfactory understanding of an existing codebase without making (or at least
imagining making) changes to it either. This led me to seriously consider that purposeful
change-driven reading might actually be more effective for understanding a program than purposeless
recreational code reading.

Unlike prose, a program lacks a linear narrative structure for reading. Sentences and paragraphs
proceed linearly, whereas code flow jumps and branches via function calls, if-statements,
for-loops, and other control structures. Even worse, given different data, the same program can
produce many different possible paths through its logic. At best, a large program's logic reads
like an incredibly complex choose-your-own adventure novel with nested sub-adventures. Given this,
we shouldn't try to derive our strategies for effectively reading programs from our strategies for
effectivey reading prose. 

Reading code is more like exploring paths in a forest. Like code, paths branch, recombine, and loop
around. Like in a large codebase, in a large forest, getting from one place to another
(understanding functionality from the outside) requires filtering out irrelevant information as
much as it does tracing one path in detail (understanding the internals of a module).

The forest metaphor also improves upon Seibel's comparison between codebases and biological systems
by acknowledging the human source of codebases. As Kartik points out in his critique of Seibel's
comparison between code and biology, while a biology metaphor is useful for illustrating how messy
large codebases can become, codebases fundamentally differ from biological systems because we
design them ourselves. The paths through a forest comparison works better because it reminds us
that a human-created system can still become difficult to understand.

With the forest metaphor in hand, let's circle back to effective code reading strategies. If
successful code reading in a mature codebase requires finding the right paths to explore and not
getting lost while doing so, we need a strategy that helps us filter out irrelevant information
continuously as we read.

Having a change to make to a codebase in mind while reading provides one such strategy for
successful codebase exploration. A change to make guides our reading by posing a simple high-level
question, "do I need to understand this in order to make my change?" and more granular lower-level
questions, "how much do I need to know about X to do Y?", "can this commit show me how to do
something I don't know how to do?", and "could this line have caused the bug I'm trying to fix?" As
a concrete example, let's say I'm tasked with adding the `open` syscall to the Linux kernel. Even
without knowing much about the Linux kernel codebase, I can confidently filter out a majority of
the codebase as irrelevant at the data flow level. I don't need to know how the kernel communicates
with keyboard drivers, how the kernel tracks battery levels, or how the Linux kernel schedules CRON
jobs. On the other hand, I will probably need to understand the generic syscall interface, how the
kernel tracks stored files, and may want to look at prior changes that added syscalls to the
kernel.

In forest terms, having a change in mind is like having a partially-filled out map. As a reader, I
start with a map that only includes my current position and an X marking my final destination (we
could enrich this further by adding that partially filled out paths on the map represent prior
knowledge of the code). From the beginning, I know that some paths aren't worth exploring at all
and others are promising. As I explore more, I draw in more paths on the map, always keeping in
mind my final destination (this reminds me of Grothendieck's [rising sea (quote on the first
page)](http://case.edu/artsci/phil/RisingSea.pdf)). And despite not knowing where all paths lead,
I can confidently wander and make partial progress. Even if we follow what ultimately turn out to
be dead ends, we can always backtrack and re-orient towards our ultimate destination.

The forest, path, and map analogy also helps justify Kartik's and my shared intuition that
discussions of clean code and interfaces ignore the things that actually make unfamiliar code
accessible to outsiders. Clean, solidified abstractions are like well-marked, easy-to-follow paths
through a forest - very useful if they lead in the direction you need to go, but less useful when
you want to blaze an arbitrary new path through the forest. If we want to optimize for arbitrary
changes by outsiders, we should focus on showing how to create new paths. I'll leave a more
practical discussion of how to do this for a later post.

Back to my original point, we now have some intuition for why change-driven reading can work better
in situations where the challenge of filtering irrelevant information overshadows the challenge of
understanding functionally isolated modules.

# Postscript
When I initially went to write this post, I had a whole follow-up section here about applying
change-driven reading in practice, since I never discussed how to decide what change make good
reading guides! In this section, I would've discussed sequencing changes from smaller to larger
scope and other change-selection strategies, but I hesitated to include it because I suspected that
most programmers already manage to sequence changes for learning effectively. After google-ing
around and talking to a coworker, I discovered that other people have already covered the "how" of
learning by making changes anyway. In [How To Be A
Hacker](http://www.catb.org/esr/faqs/hacking-howto.html#idm45418026893328), Eric Raymond discusses
what he calls the "The Incremental-Hacking Cycle", a process by which someone gradually expands
their understanding of a codebase by making bigger and bigger changes to it. Even more interesting,
David Maciver recommends a very similar approach for learning math in [a post about how to read
math textbooks](http://www.drmaciver.com/2016/05/how-to-read-a-mathematics-textbook/). Finally, my
coworker, an algorithms wizard, mentioned that he understands proofs, algorithms, and frameworks by
trying to find logical holes or exploits for them. So if you're stumped on how to find the right
change to guide your reading, read these posts and then [let me
know](mailto:stephenmalina@gmail.com) if you're still stumped after that.

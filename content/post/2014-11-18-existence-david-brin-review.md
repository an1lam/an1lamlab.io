---
date: 2014-11-18
date: 2014-11-18
layout: post
title: Book Review - Existence by David Brin
category: posts
draft: false
---

Mind-blowing, challenging, and disturbing at times, David Brin's view of the future is more comprehensive and more diverse than any other writer's. The thing I love about this book is that it doesn't shy away from discussing hard truths, like the myriad failure-modes humanity can enter. However, it accompanies this discussion with a constant sense that, if it's possible, humanity will find a way.

<i>[Existence](http://www.davidbrin.com/existence.html)</i> is a sci-fi novel for the Internet Age. This shows through in the way the novel plays with the idea of multi-tasking. First, Brin attacks the attention deficit we all possess head-on by showing how augmented reality and constant stimulation have been taken to their logical conclusions in the world he has crafted. On a higher level, chapters tend to be short and jumpy, often leaving the reader frustrated, mirroring our frenetic culture. In the end though, Brin manages to make it work, weaving a quilt of stories that slowly but surely come together together into a coherent worlds-spanning narrative.
 
What truly sets this novel apart from many classic sci-fi novels are the characters. Brin's characters are nuanced, defying good or evil archetypes for the most part. While there obviously are characters who we root for throughout the novel, other characters start off as hateable, and become characters with whom we feel kinship.

I can't recommend <i>[Existence](http://www.davidbrin.com/existence.html)</i> enough, both as a glimpse of the future of humanity and a piece of modern literature.

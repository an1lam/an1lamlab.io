
date: 2018-06-26
date: 2018-06-26
- Sheds interesting light on expert performance.
- What about cognitive biases? If the brain is a prediction machine, then why do we fall prey to "sub-optimal" biases?
- Evolutionary argument against Plato's cave: prediction is so important for brains that if we were only perceiving the shadows on the wall, some other animal would have eaten us all by seeing the actual fire.
- What does this theory predict that others don't? God, that question's meta in this context.
- Should read more about Geoffrey Hinton's early un-supervised learning work.
- Can this framework explain hunger and addiction?
- What frameworks does this framework compete with?
- Should look into Rao & Ballard's "hierarchical network of 'predictive estimators'" paper to better understand toy predictive processing model better.
- Does this framework require that neurons die and get replaced or only that synapses do?
- Key takeaway of embodiment framework is that some environments will reward prediction accuracy more, whereas others will reward environment optimization. Which type is entrepreneurship?

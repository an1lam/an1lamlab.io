---
date: 2014-11-26
date: 2014-11-26
layout: post
title: Book Review - Regenesis by George Church
category: posts
draft: false
---
Synthetic biology is a burgeoning discipline that stems from a long lineage of genetic meddling. Synthetic biology's ancestral legacy began when man first selected which seeds to plant based on the traits of the plants that birthed them. In [Regenesis](http://www.amazon.com/Regenesis-Synthetic-Biology-Reinvent-Ourselves/dp/0465075703/ref=sr_1_1?ie=UTF8&qid=1416946667&sr=8-1&keywords=regenesis) by George Church, Church takes the reader through this lineage, canvassing the past thousands of years and then focusing in on the past 30.

Church does a good job of balancing technical explanations with interesting anecdotes in the book. Some of the explanations of genetic delivery and manipulation techniques pushed the boundary of what a reader with no biology background could comprehend without consulting outside sources, but never crossed that boundary. Fortunately, Church also displays surprising narrative ability during various discussions of evolution and early genetic meddling by farmers,

>"The original ancient text is written in the genomic DNA of every being alive today. That text is as old as life itself, and over 1030 copies of it are distributed around the earth, from 5 kilometers deep within the earth’s crust to the edge of our atmosphere, and in every drop of the ocean. A version of this text is found in each nucleated cell of our bodies, and it consists of 700 megabytes of information (6 billion DNA base pairs). It contains not only a rich historical archive but also practical recipes for making human beings. For such a significant text, its translation into modern languages began only recently, in the 1970s." (pg. 38)

Another place in which Church excels is in discussing the safety and security concerns of these technologies. As a consummate optimist, I am  admittedly more sympathetic than average towards the acceptance and propagation of these technologies. But, in the context of people like Francis Fukuyama and Bill Bryson decrying all forms of technological progress that attempt to enhance humans, I found Church's cautious but firm support of transparency, progress, and community involvement in science refreshing and needed. I found Church's justification for human enhancement compelling and in line with my own thoughts on the topic,

>"There is already a wide variation in talents among members of the human race, but those with great intelligence, strength, and good health do not claim special rights for themselves on that account. Conversely, those with severe physical and/or mental disabilities are nevertheless accorded full human rights by the legal structures of enlightened democratic governments." (pg. 228)

My only gripe about Regenesis is related to the final section of the book where Church discusses the future of synthetic biology and society broadly. Church neglects to delve into the diverse possibilities he promises will be yielded as a result of these technologies' advancement. This leaves the imagination of future applications of these technologies primarily as an exercise for the reader. This was disappointing because Church, as a world authority on these technologies and as someone who has a history of thinking big, is in the perfect position to speculate on what wonderful outcomes the proliferation of synthetic biology may bring. However, like many scientists before him, for Church, modesty overcomes unbridled imagination.
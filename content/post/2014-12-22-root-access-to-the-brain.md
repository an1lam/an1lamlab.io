---
date: 2014-12-22
date: 2014-12-22
layout: post
title: Gaining Root Access to the Human Brain
category: posts
draft: true
---
For a long time, I've been fascinated by meditation and other
internally-focused arts. Although I've mainly practiced mindfulness
meditation, I've spent far too many hours reading about myriad types of
meditations, breathing exercises, and visualization practices.

I have a confession to make. I'm not seeking enlightenment.

I don't care about piercing the veil of perception in order to see the
true reality and merge with the Universal Mind, or whatever your Eastern
philosophical doctrine proposes will happen as a result of
Enlightenment. Those are worthy aims. They just aren't *my* aims. No, I
am interested in a much more earth-bound
achievement. I'm interested in gaining root access to the human brain
(and through the brain, the body).
\* Disclaimers: 
* from here on out I'm going to be using many programming /
  Unix-like metaphors for what I'm talking about. If you find yourself
  lost, stop and look at the *Glossary* I've provided below.
* If you don't believe that the brain can be explained using strictly
  physical ideas, you should stop reading now. I do not intend to engage in
  metaphysical arguments about the brain's physicality or about its
  reducibility to a universal computing device \*
I've come to the conclusion that our daily experience of consciousness
is analogous to the experience of a typical computer user who spends
their time in user land. Although the analogy is a little
fuzzy, it will become more clear what characteristics the two
experiential modes share soon. The majority of computer users see their
computer as an unfixable tool with mystical innards. They don't know how
their computer works, and they don't know how to fix their computer if
it breaks. Similarly, there are no humans (that I know of) who are able
to consciously fix their brains through effort.

At this point, if you possess a certain level of familiarity with
computers, brains, or both, you've probably come up with a few
objections. If you're in the first group and you're not totally opposed
to the whole analogy, you may still be frustrated that I'm ignoring the
distinction between hardware and software. This is an important point.
Even the most skilled systems person can't fix a damaged hard drive from
their terminal. In a more general sense, without nanotechnology or advanced robotics, software can't manipulate
matter at a fine scale (at least not in your personal computer).
However, in the brain, this argument lacks veracity. The brain has the
advantage of having control over nano machines, pumps, and other
contraptions that provide it with self-targeting repair capabilities.
This is indicated by the fact that functioning brains routinely clean
themselves of trash molecules through a process called autophagy (the
breaking down of dead cell materials.)
Therefore, in my analogy, I'm going to imagine your personal computer as
a futuristic device with little nano machines inside it that can fix
different parts of it. With this conceit, our analogy becomes stronger.
Unfortunately, I'm not yet sure what the second group, the neuroscience
and psychology experts, will object to, so I can't address their
objections pre-reading. However, I encourage those of you who have
brain-related objections to contact me with them.

So, now that I've hopefully at least made you feel better by attempting
to address your objections, let's get back to the main focus. As you've
probably already figured out, my goal is to gain control of my brain's
lower level processes. A good example of a 'low-level process' we
already possess control over is breathing. I view this as the likeliest
reason why various meditative traditions instruct their practitioners to
focus on their breath.

Our control over the breath provides an exemplar for my vision of
controlling the brain's low-level processes. Normally, the breath is
automatic. We don't die when we stop focusing on our breath. However,
control of the breath can become conscious with little effort. I simply
think about breathing at a certain speed, and it happens. Furthermore,
when controlling my breath, I receive feedback quickly on my body's
current state. The feeling we get when we hold our breath for too long
indicates to us that it is time to breath. Although this indicator is
not as accurate as CO2 and oxygen gauges, it serves us well.

Using the breath as a model, we can imagine what controlling other
low-level processes would be like. For example, what if we could control
our heartbeat with the same level of precision as our breath? Currently,
we can only affect our heartbeat indirectly, through the breath,
positioning, and prior activities. If we could instead lower and raise
the heartbeat at will, things would be drastically different. Every time
we'd want to de-stress we'd slow down the breath and our heart until we
reached a state of deep relaxation. Often, stress starts with 
a release of norepinephrine and adrenaline leading to increased
breathing and heart rates. If we could consciously suppress these
processes, we'd possess a more pronounced ability to modulate our stress
levels. All sorts of interesting, outlandish ideas arise if we continue
to ponder the idea. People experiencing extreme cold could override
their natural response to cold (a slowing down of the heart beat, \*
need citation here \*), and instead increase their heart rate in order
to increase heat generation in their bodies.

At this point, you're probably thinking "What about the risks?" This
question is totally valid. Noone wants the next headline to be "10 year
old speeds up heart until it fails through new brain hacking technique".
The key mechanism that will prevent these risks from being an overriding
concern is the feedback mechanism. In the same way it is vital that our bodies tell us
when we need to breath more air, it is important that any method of
consciously affecting our heart rate allows for a feedback mechanism
which protects us from unexpectedly killing ourselves. Unfortunately,
the only way to discover if this feedback mechanism is encoded into our
biology natively is to first figure out how to control our heart rate.
Since controlling our heart rate would be a new ability for humans, it
may not possess the same feedback mechanism as controlling our breath
does. If this is the case, we will exercise extra case in experimenting
with this new technique. But, the worst case scenario should not prevent
us from discovering the ability in the first place.

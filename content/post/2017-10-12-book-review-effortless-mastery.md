---
date: 2017-10-12
date: 2017-10-12
layout: post
title: Book Review of Effortless Mastery
category: posts
draft: true
---

Effortless Mastery is a strange book, one-third meditation guide, one-third musical exercises, and one-third amateur (seriously amateur) metaphysics sermon. Reading Effortless Mastery reaffirmed my growing conviction that some books just aren't meant to be read all-the-way-through from front to back. As a non-musician, mystical skeptic, and fox-in-training, I have little in common with the author of Effortless Mastery, Kenny Werner. Moreover, I suspect Werner, a professional musician, Advaita Vedanta adherent, hedgehog (by my assessment, I doubt he's aware of the fox-hedgehog dichotomy), wouldn't like me, even if we ignore my brief attack on his only published work.

All that said, Effortless Mastery makes up for all its quirks, fluff, and half-baked metaphysics because it captures an important concept that I've never been able to grok, "that which is worth doing, is worth doing effortlessly." Werner's connection of effortlessness to musical playing and practice resolves a dichotomy that writings on Taoism (I've read this one, and this one), Flow, and deliberate practice all fumble with, the dichotomy between intentional growth and effortlessness. 

## The Tension Between Flow and Mastery
If you read enough Taoist and psychology books that discuss expertise, you'll eventually notice writers in the space adhere to one of three schools-of-thought about what type of action creates and reinforces mastery: the "just go with the flow" school, the "push, push, push" school, and the "push until you make it" school. 

In the "go with the flow" school, I place Taoists together with Mihaly Csikszentmihaly (one of two recursive names I've seen), so widely cited at this point that he's earned his own category. Both Taoists and Csikszentmihaly argue that growth occurs as a side effect of the flow state, often called wu-wei in Taoism.


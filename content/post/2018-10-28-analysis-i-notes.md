---
date: 2018-10-16
layout: post
title: Notes on Terry Tao's "Analysis I"
category: posts
draft: true
---

## Chapter 2
### Notes
In which I sketch out some proofs and copy in the relevant Peano Axioms along the way.

#### Proof: The sum of two natural numbers is also a natural number.
**Axiom 2.1.** 0 is a natural number.  
**Axiom 2.2.** If n is a natural number, then $n{+}{+}$ is also a natural number.  
**Axiom 2.5.** For property, $P(n)$, if $P(0)$ and, for arbitrary natural number n, $P(n)$ implies $P(n{+}{+})$, then $P(n)$ is true for all $n \in N$.  
**Definition 2.2.1 (Addition of natural numbers).** Let m be a natural number. To add 0 to m, we define $0 + m := m$. Assume we've defined how to add n to m, let us define $(n{+}{+}) + m := (n+m){+}{+}$.  
**Proposition.** For any $n \in N$, with fixed $m \in N$, $n+m$ is a natural number.  
**Proof.** Assume $m$ is a fixed natural number. $0+m$ is a natural number because $0+m:=m$. Inductively, assume $n+m$ is a natural number. $(n{+}{+})+m=(n+m){+}{+}$. We know $n+m$ is a natural number because of the inductive hypothesis. By Axiom 2.2, $(n+m){+}{+}$ is also a natural number.  
**Question for QC.** Is this a valid proof? How come I didn't have to use Axiom 2.1?

#### Proof: For any natural number, n, n+0 = n.
**Proof (By induction).** When $n = 0$, $0 + 0 = 0$ by definition of addition. If by inductive hypothesis, $n + 0 = n$, then $(n++) + 0 = (n+0)++ = n++$.

### Exercises


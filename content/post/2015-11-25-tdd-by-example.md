---
date: 2015-11-25
date: 2015-11-25
layout: post
title: Weekly Newsletter
category: posts
draft: false 
---

## Value Objects
* Avoid *aliasing*, where changes to mutable objects cascade to connected
  objects.
* All value objects must return new objects
* Should implement an equals method

## Triangulation
* Only generalize code when we have two or more examples (enough to triangulate
    for a radio signal)
* Ignore duplication until our tests require us to generalize
* Useful when you want to think about the "axis of variability"
## Decouple
* Make the tests use an interface that is decoupled from the underlying
    implementation.
    * Ex: Money.dollar vs. new Dollar
        Using the 1st one allows us to change the implementation of dollar
        without affecting the test
## Dedupe
* After the tests pass, try to see where we can dedupe our implementation

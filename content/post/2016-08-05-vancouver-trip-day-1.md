---
date: 2016-08-05
date: 2016-08-05
layout: post
title: Vancouver Trip - Day 1
draft: true
category: posts
---
We got an early start, heading to the airport at 4:15 AM and taking off at 7:30 AM.

During the flight, whilst I listened to Ben Franklin's Autobiography and the Scrum book, Jen interrupted me to point out a much-appreciated unsullied view of Mt. Baker.

<img src="https://dl.dropboxusercontent.com/s/5ymwu38vaklkxuj/2016-08-04%2013.07.22.jpg?dl=0">

After reading about [fasting as a preventative for jet lag](http://harpers.org/blog/2012/03/the-empty-stomach-fasting-to-beat-jet-lag/), I resolved to fast from Wednesday night's dinner to mid-day of our arrival in Vancouver. I succeeded at this, further convincing Jen's mother of my strangeness.

We arrived in Vancouver and found that Vancouver's weather gods rewarded our early wake-up with beautiful weather. After dropping our stuff at the hotel, we headed to Granville Island to eat and appreciate the "natural" market. Granville market sports farmers markets and cute shops, targeting self-aware but helpless charm-seeking tourists like ourselves. Arriving there, we all ooed and awed at the outdoor-indoor combined market. Throughout the trip, I kept coming back to the fine line between intentional and emergent charm, beginning with the Granville Market and extending throughout our time in Vancouver, a city as close as I've seen to authentically charming. My obsession with intentional and emergent charm comes from my struggle to define and put a value on authenticity. On one hand, I reject the dichotomy between authentic and artificial from an intellectual standpoint. On the other hand, I can't with a straight face argue that I see no difference between a farmer's market and the Gristedes by my own apartment, independent of the quality of produce the two places sell. As a result, I get caught up in unproductive thought loops where I first appreciate something that strokes my pattern recognition modules for charm, notice the arbitrary triggering of those modules, and then question what it means that I'm so easily woo-ed by such things (I feel bad for the people forced to hang out with me too, don't worry).

<img src="https://dl.dropboxusercontent.com/s/x7cykg0q110xoev/2016-08-04%2012.43.56.jpg?dl=0"></img>

We briefly explored the market and then beelined to the descriptively named [Tony's Fish and Oyster Cafe](http://www.tonysfishandoystercafe.com/). There I managed to find the one other obnoxious person like myself and cut her in line to the bathroom. It's worth noting that I have a kitten bladder and use the bathroom between 3 to 5 times more than Jen or her family. Yes, I'm serious. Yes, I counted.

After our satisfying lunch, we did a second run-through of the Granville Island farmer's market. As we walked through the market, I felt my envy of Vancouver grow. I have a soft spot for local or, if you're cynical, local-seeming food production, a less reasonable, accessible option in NYC. Given this, the Granville Island market checks all the localvore boxes. You've got the faux health food stands, the small farm produce stands with arranged fruit bouquets, and finally the straight-up unhealthy but quality ingredient pizza stands and Italian deli counters.

From Granville Island, we crossed through the city to Stanley Park to walk the Seawall. The cab driver transporting us repeatedly reminded us that the Seawall ran 8km, but we largely ignored his reminders. Stanley Park's another enviable aspect of Vancouver's city life. While I love Central Park, the number of other people around and easy access to the city prevent me from feeling as though I'm truly out in nature, artificial distinction or not. Stanley Park, on the other hand, provides whatever intangibles I seem to require to feel disconnected from civilization.

In the end, the Seawall was a beautiful but surprisingly, or unsurprisingly if you're the taxi driver, tiring 6 mile walk. As we walked around the Seawall, I mused in my usual way, subjecting Jen to summaries of books recently read, thinking about our relationships to cities, and parroting recently learned, poorly researched facts about differences between Western and Native American languages, prompted by the totem pole reconstructions.

![First view of the city from the Seawall walk](https://dl.dropboxusercontent.com/s/evn32nf3s29njds/2016-08-04%2015.34.01.jpg?dl=0)
![Reconstructed Kwak- waka'wakw tribe totem poles](https://dl.dropboxusercontent.com/s/j0h6lf6j3g2bk05/2016-08-04%2015.52.25.jpg?dl=0)

I'm obsessing about my and our [society's] relationship to cities because I can't untangle all my different feelings about cities, New York in particular. As a technologist, I appreciate the convenience factors - you can walk everywhere and you can always find food - and technological density of cities. As an introvert, I respect the isolation and slower pace of Hanover, my former home. As an uninformed but overly confident speculator about society, I can't help but notice the inexorable march of the rural population to cities.

Given all of the above, I'm afraid because I firmly believe that cities contribute to the growing consumerization of our culture. As a staunch capitalist, I love that we use money to improve our lives, but the individualist and [Lazarus Long fan](http://kottke.org/08/01/a-human-being-should-be-able-to) in me fears the over-specialization of city inhabitants.

Let's take food as a microcosm. Among my peers, 90% eat out more than they cook (potentially by a factor of 2 or more). Cooking is time and labor-intensive, and I'm all for trading time for money (more time, less money). However, the health and skill implications worry me. Going back to my point about authenticity, the same part of me that prefers farmers markets to Gristedes fears the world in which we trust restaurants to source and cook all of our food and in which the average citizen doesn't know how to cook more than one or two dinners.

I want to get back to the travel stuff, but I'll conclude this digression with a disclaimer that I can think of at least two thoughtful friends who can make good arguments against the above to which I have no rebuttal.

![The many facets of Vancouver: Barges, Seaplanes, Mountains and Bridges](https://dl.dropboxusercontent.com/s/tg7b9aef1c7brxg/2016-08-04%2016.44.59.jpg?dl=0)
![Our Friend, the Harbor Seal](https://dl.dropboxusercontent.com/s/dg0daljw0da0ipm/2016-08-04%2017.00.15.jpg?dl=0)

From the Seawall walk, we rushed to our dinner at a Japanese restaurant. Despite none of us eating sushi, we found great options at the restaurant.

![To-be-cooked Kobe Beef Strips with a Piping Hot Rock](https://dl.dropboxusercontent.com/s/7s9t5ik74cmlgb0/2016-08-04%2018.31.18.jpg?dl=0)
![Rice Balls with a Twist](https://dl.dropboxusercontent.com/s/iyg9gyob09e31pa/2016-08-04%2019.01.00.jpg?dl=0)
![Bibimbap](https://dl.dropboxusercontent.com/s/c5qplu3z2nubeb3/2016-08-04%2018.20.30.jpg?dl=0)

Before sleep deprivation ran its course, we visited the Vancouver lookout, a spire overlooking the city. While there, learning about Vancouver's massive port prompted some idle thinking about how ports fit into a world where bits replace atoms. As a software engineer, I focus on problem spaces where bits already reign supreme, spaces which can feel saturated and unimpactful (I would say "no offense to social networks", but I mean to offend them). Fortunately, seeing the amount of commerce based around the physical transportation of raw materials reminded me that software can also empower people moving and interacting with physical materials, be it through improving a barge captain's ability to navigate or automate a dangerous aspect of mining. I'm excited by a future in which software empowers people to perform better and frees us from dangerous or dehumanizing tasks.

![Vancouver Skyline from the Spire](https://dl.dropboxusercontent.com/s/jsaqxchfmsf7nxd/2016-08-04%2021.52.27.jpg?dl=0)

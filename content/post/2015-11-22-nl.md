---
date: 2015-11-22
date: 2015-11-22
layout: post
title: Weekly Newsletter
category: posts
draft: false 
---
## Education
[A Radical Way of Unleashing a Generation of Geniuses](http://www.wired.com/2013/10/free-thinkers/)  
Phenomenal piece that profiles an unconventional teacher in a poor Mexican border town. Uses this to broadly discuss the changing role and viewpoint on education. I'm a total convert to the idea that children need to be allowed to explore rather than have information forced down their throats.

## Business
[When Software Eats Bio](http://a16z.com/2015/11/18/bio-fund/)  
The leader of Andreessen Horowitz new bio arm gives a Q&A about the motivations and direction they will take.

## Neuroscience
[Single Artificial Neuron Taught to Recognize Hundreds of Patterns](http://www.technologyreview.com/view/543486/single-artificial-neuron-taught-to-recognize-hundreds-of-patterns/)  
Profiles a new discovery about how neurons represent information. May explain why and how chunking happens in the brain. Chunking is the scientific term for the reason why you can remember a sentence more easily than 7 numbers in sequence.

## Other Contributions
[The Misleading Video Interview With a Rapist at the Heart of the Campus Sexual Assault Freakout](https://reason.com/archives/2015/11/20/lisak-frank-interview-problem-rape/)  
Baird sent me this description along with the article:

> "Reason continues to examine the background behind claims made by David
> Lisak, a widely-cited "expert" on rape. I use quotation marks because, as
> this piece makes clear, Lisak has not conducted original research on the
> topic in thirty years, and the claims he makes are based on an intentional
> misrepresentation of his dissertation. We should all want to prevent sexual
> assault, but creating a narrative meant to provoke visceral reactions at the
> expense of factual evidence does that cause a disservice.  That David Lisak
> has done so (to his profit) for the past three decades will hopefully lead
> people to question his influence."

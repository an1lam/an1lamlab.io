---
date: 2015-10-23
date: 2015-10-23
layout: post
title: Sample draft post
category: posts
draft: true
---
I want to turn my brain into something that can better process code. And I want
to be able to better manipulate my lower-level semantic networks. Possible
avenues for this include visualization, deconcentration, and meditation. First,
I will elaborate upon what succeeding at these goals would mean. With my desired
result laid out, I will circle back and discuss the extent to which each
possible avenue satisfies the criteria. Finally, I will establish a path for
getting to my goals. Achieving these goals will require a novel, iteratively
developed practice.

I have two obsessions: the mind and the machine. I consider the body a
projection of the mind, and therefore train my body to train my mind. The mind
is the container for experience. My personality tends towards reductionism. As a
reductionist, I cannot help but focus on controlling the container for my
experience. 

I'll start with meditation since meditation is where I began my journey into
this area of brain hacking. Fittingly as the first step in a long journey,
meditation is the furthest from what I'm looking for. In the typical
incarnation, stripped of all the fluff, meditation trains concentration and
patience. Meditation requires focusing on one somatic experience (breath)
continuously. It requires the practitioner to select this sensory experience
above all else and wait for the experience to become enjoyable. Surprisingly,
this simple practice produces measurable positive changes in the practitioner's
brain. 



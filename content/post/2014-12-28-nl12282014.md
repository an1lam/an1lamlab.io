---
date: 2014-12-28
date: 2014-12-28
layout: post
title: Weekly Newsletter
category: posts
draft: false
---
# Stoicism
Stoicism has been at the forefront of my thoughts this
break. I'm most of the way through Seneca's
<a
href="http://www.amazon.com/gp/product/B005NC0MGW/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B005NC0MGW&linkCode=as2&tag=stepmali-20&linkId=IGHNRBFOQW6NNUA6">Letters
from a Stoic: Epistulae Morales AD Lucilium</a><img
src="http://ir-na.amazon-adsystem.com/e/ir?t=stepmali-20&l=as2&o=1&a=B005NC0MGW"
width="1" height="1" border="0" alt="" style="border:none !important;
margin:0px !important;" />, a primary source for Stoic
philosophy
and recently finished Ryan Holiday's
<a
href="http://www.amazon.com/gp/product/B00G3L1B8K/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00G3L1B8K&linkCode=as2&tag=stepmali-20&linkId=BZH2FGJUDGAOKXFW">The
Obstacle Is the Way</a><img
src="http://ir-na.amazon-adsystem.com/e/ir?t=stepmali-20&l=as2&o=1&a=B00G3L1B8K"
width="1" height="1" border="0" alt="" style="border:none !important;
margin:0px !important;"/>, which presents a simplified
version of Stoicism that caters to modern audiences.

The first article about Stoicism in this newsletter, [Indifference is a
power](http://aeon.co/magazine/philosophy/why-stoicism-is-one-of-the-best-mind-hacks-ever/),
reveals Stoicism's tenets and discusses its application both
by its founders during Roman times and in modern cognitive behavioral
therapy (the creator of Cognitive Behavioral Therapy struggled as a
psychoanalyst until he remembered the teachings of ancient Stoicism,
[citation](http://www.prospectmagazine.co.uk/features/albertellis).)
This article surpasses prior articles I've included on Stoicism by
providing a unique combination of fascinating anecdotes about practicing
Stoics and practical techniques for applying Stoic ideals to your life.

In the first article, the author cites Vice Admiral Stockdale,
a former Navy commander and Vietnam prisoner of war, as an archetypal example
of a modern Stoic. Intrigued, I
tracked down the speech he mentions in the article: [Courage under Fire:
Testing Epictetus's Doctrines in a Laboratory of Human
Behavior](http://media.hoover.org/sites/default/files/documents/978-0-8179-3692-1_1.pdf).
The author's story acts as a case study for how to practice Stoicism
when life becomes hell and provides an ultimately inspiring story
about overcoming great hardship.

# Neuroscience
[The Mind's Eye: What the blind see.](http://powers.media.mit.edu/wiki/upload/MindsEye.pdf)  
Oliver Sacks contrasts two cases of brain changes resulting from
adult-onset blindness.  I'm fascinated by the effect blindness, or any sensory
deprivation for that matter, has on the brain, and I found this
study fascinating for two reasons. First, Sacks tells of a man who
lost his sight but continued to visualize intensely and to build complex
visual models of his surroundings -- an ability that appears to have
applications outside of this narrow scenario. Second, after discussing this
case and one other, Sacks introduces the profound idea that, below
visualization, a more fundamental language of thought may exist.  I'd
never considered this possibility before, and it's certainly rattled my
model of how the brain works.

# Biology
[The Surprising Power of an Electric Eel's Shock](http://www.nytimes.com/2014/12/04/science/the-surprising-power-of-an-electric-eels-shock.html)  
Dissects the mechanisms behind the electric eel's shock and expounds
upon the tactics electric eels use to hunt and catch their prey.

[A Mini Farm That Produces Food From Plastic-Eating Mushrooms](http://www.wired.com/2014/12/mini-farm-produces-food-plastic-eating-mushrooms/)  
Profiles the work of a team of designers and scientists, who are
engineering mushrooms that convert plastic to food, or at least harmless
organic material.  Our understanding of mushrooms and their complexity
continues to grow and this study shows the potential for increased
practical applications. To put this in
context, imagine if, in addition to compost, you had a plastic digestor
bin in your house. You'd place plastic bottles and wrappers into the
bin and close the lid. Then, a few days later, you'd open the bin's lid
and find edible mushrooms. This sounds eerily similar to how I've always
imagined the future of food production.

# Preparedness
[I will survive](http://www.economist.com/news/christmas-specials/21636611-when-civilisation-collapses-will-you-be-ready-i-will-survive)  
Discusses the culture of survivalism in America. Presents a balanced
view on an oft-mischaracterized group.

# Other Contributions
[Why is everyone so busy?](http://www.economist.com/news/christmas-specials/21636612-time-poverty-problem-partly-perception-and-partly-distribution-why) (David Wylie)  
Accompanying this article, David sent me this blurb, "I've been
intrigued by the changing dynamics of the labor/leisure tradeoff and
this article does a really good job of analyzing the changing value of
"free time" as economies grow and how it's perceived and distributed." I
share David's fascination with the "changing dynamics of the
labor/leisure tradeoff" as it's a phenomenon that writers often allude to but
rarely explain. More importantly, it's a phenomenon with which almost all
of us will or already do experience firsthand.

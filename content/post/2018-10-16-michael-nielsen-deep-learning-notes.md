---
date: 2018-10-16
layout: post
title: Notes on Michael Nielsen's "Neural Networks and Deep Learning"
category: posts
draft: true
---
# Chapter 1
## Notes
- How far can perceptron's take us? How much do Marvin Minsky's objections to perceptrons apply to sigmoid neurons?
- [For QC]: How would I, if I were inventing logistic neurons for the first time, know to propose the sigmoid function? What properties would I look at?
- [For QC]: How much do you rely on reasoning in your head vs. writing things out, especially when you're reading?
- [For QC]: Ratio of thinking to reading, how much time do you spend on each point?
- If we start with,
    
    \begin{eqnarray} 
      \mbox{output} = \frac{1}{1+\exp(-\sum_j w_j x_j-b)}.
    \end{eqnarray}

    how do we end up with

    \begin{eqnarray} 
      \Delta \mbox{output} \approx \sum_j \frac{\partial \, \mbox{output}}{\partial w_j}
      \Delta w_j + \frac{\partial \, \mbox{output}}{\partial b} \Delta b,
    \end{eqnarray}

    ? It makes intuitive sense that the change in output would be the change in weight times the partial derivative of output over weight, since this represents the amount output would change given a minute perturbation of weight, but I wouldn't know how to derive this.
- The gradient is the transpose of the partial derivative of a vector, i.e.,

    \begin{eqnarray} 
      \nabla C \equiv \left( \frac{\partial C}{\partial v_1}, 
      \frac{\partial C}{\partial v_2} \right)^T.
    \end{eqnarray}
- Why does the change in $C$ equal,

    \begin{eqnarray} 
      \Delta C \approx \frac{\partial C}{\partial v_1} \Delta v_1 +
      \frac{\partial C}{\partial v_2} \Delta v_2?
    \end{eqnarray}

## Exercises
**Sigmoid neurons simulating perceptrons, part I**  
Suppose we take all the weights and biases in a network of perceptrons, and multiply them by a positive constant, $c>0$. Show that the behaviour of the network doesn't change.

Let's start with a single perceptron. How will its behavior change?

A perceptron's output function is,

\begin{eqnarray}
  \mbox{output} = \left\\{ 
      \begin{array}{ll} 
          0 & \mbox{if } w\cdot x + b \leq 0 \\\\\\
          1 & \mbox{if } w\cdot x + b > 0
      \end{array}
  \right.
\tag{3}\end{eqnarray}

The same perceptron with its weights and biases multiplied by a positive constant will have output function,

\begin{eqnarray}
  \mbox{output} = \left\\{ 
      \begin{array}{ll} 
          0 & \mbox{if } c (w\cdot x) + c \cdot b \leq 0 \\\\\\
          1 & \mbox{if } c (w\cdot x) + c \cdot b > 0
      \end{array}
  \right.
\tag{3}\end{eqnarray}

If we then pull the $c$ out of the equation further,

\begin{eqnarray}
  \mbox{output} = \left\\{ 
      \begin{array}{ll} 
          0 & \mbox{if } c (w\cdot x + b) \leq 0 \\\\\\
          1 & \mbox{if } c (w\cdot x + b) > 0
      \end{array}
  \right.
\tag{3}\end{eqnarray}

Then we see that the function result being compared to 0 will be multiplied by a constant factor. Numbers less than 0, when multiplied by constant factors, are always still less than 0 and positive numbers, multiplied by constant factors, are always still greater than 0. Multiplying weights and biases by $c$ has no impact on the output of a given perceptron. Multiplying all perceptrons' weights and biases by $c$ will also have no impact on a network's final output.

**Sigmoid neurons simulating perceptrons, part II**
Suppose we have the same setup as the last problem - a network of perceptrons. Suppose also that the overall input to the network of perceptrons has been chosen. We won't need the actual input value, we just need the input to have been fixed. Suppose the weights and biases are such that $w \cdot x + b \neq 0 $ for the input $x$ to any particular perceptron in the network. Now replace all the perceptrons in the network by sigmoid neurons, and multiply the weights and biases by a positive constant c>0. Show that in the limit as c→∞ the behaviour of this network of sigmoid neurons is exactly the same as the network of perceptrons. How can this fail when w⋅x+b=0 for one of the perceptrons?

In the case where $ w \cdot x + b \neq 0 $, $ c ( w \cdot x + b ) $ will approach $ \infty $ if $ w \cdot x + b > 0 $ and $ - \infty $ if $ w \cdot x + b < 0 $, giving us the same behavior we'd expect from a perceptron because $ \sigma(-\infty) = 0$ and $ \sigma(\infty) = 1$. However, in the case where $ w \cdot x + b = 0 $, $ \sigma(0) = \frac{1}{2} $, which does not match the perceptron's output of 0.

**Bitwise representation of a digit**
There is a way of determining the bitwise representation of a digit by adding an extra layer to the three-layer network above. The extra layer converts the output from the previous layer into a binary representation, as illustrated in the figure below. Find a set of weights and biases for the new output layer. Assume that the first 3 layers of neurons are such that the correct output in the third layer (i.e., the old output layer) has activation at least 0.99, and incorrect outputs have activation less than 0.01.
<img src="http://neuralnetworksanddeeplearning.com/images/tikz13.png" alt="Four layer network" />

The key to solving this problem is realizing that each output node should output an above-threshold response if the number indicated by the prior layer's output includes a 1 for this bit in its binary representation. For example, if layer 3 outputs 6, then nodes 1 (zero-indexed) and 2 should have outputs $>0.99$. The following table includes the full mapping of layer 4 node activations to layer 3 non-0 inputs.

 Layer 4 Index  | Layer 3 Indices 
:---------------|:-----------------
 0              | 1, 3, 5, 7, 9   
 1              | 2, 3, 6, 7      
 2              | 4, 5, 6, 7      
 3              | 8, 9            

Note that 0 will automatically be represented as all <0.01 activations. 

From here, we have to construct weights that replicate an OR function of each output node's inputs. To make things simple, assume we have an activation threshold of 0.5, meaning $w \cdot x + b > 0$ in order for a neuron to output an above-threshold activation. Weights will be a 4x10 dimensional vector, where output will be a 4x1 dimensional vector with each row equal to the dot product of $w[i]$ and $x[i]$. 

Given this, we can have weights,
$$
\begin{bmatrix}
0 & 2 & 0 & 2 & 0 & 2 & 0 & 2 & 0 & 2 \\\\\\
0 & 0 & 2 & 2 & 0 & 0 & 2 & 2 & 0 & 0 \\\\\\
0 & 0 & 0 & 0 & 2 & 2 & 2 & 2 & 0 & 0 \\\\\\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 2 \\\\\\
\end{bmatrix}
$$

and a bias of -1 for all inputs. This way, each output neuron will have $w \cdot x + b > 0$ when any of the layer 3 inputs to which it maps in the above table have activation $0.99$ because $ w \cdot x + b = 2 * 0.99 - 1 = .98 $, but $ w \cdot x + b < 0$ when all mapped inputs to the neuron are $ \leq 0.01 $, because $ 2 * 0.01 - 1 = -.98 $.

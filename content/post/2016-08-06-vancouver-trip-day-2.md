---
date: 2016-08-06
date: 2016-08-06
layout: post
title: Vancouver Trip - Day 2
draft: true
category: posts
---
We kicked off the day with the Capillano suspension bridge, which acts as the sole entranceway to woods on the other side of the gorge which the bridge spans.
![A View of the Bridge](https://dl.dropbox.com/s/bq6ukhzj4qsqjop/2016-08-05%2011.14.49.jpg?dl=0)
As we explored the woods beyond the bridge, the careful attachment of a tree walk's platforms to Douglas Firs prompted thinking about integrating nature and human activity, which Vancouver does well.  Small examples of this integration beyond the bridge attraction include trees on roofs of buildings, gardens nestled throughout the city, and sustainable energy sources throughout the city. When my friend visited Japan he described a similar impression, paraphrased as "the Japanese don't make distinctions between nature and civilization. They integrate gardens into cities or concrete into gardens." Vancouver's integration is a more self-aware and active one, but I respect it nonetheless.

Walking around the forest beyond the bridge also reminded me how much I like woods. I enjoyed speculating about whether trees grow from the top, the bottom, or all along their trunks, seeing the captive birds of prey, and skirting through the treetops of the Douglas Firs.
![Down the River](dl.dropbox.com/s/2777nlj446z8pok/2016-08-05%2011.17.07%20HDR.jpg?dl=0)
![Captive Hawk](https://www.dropbox.com/s/qo1cndi6xeeyup6/2016-08-05%2012.07.19.jpg?dl=0)
![Captive Owl](https://www.dropbox.com/s/twcn8httgfjsrol/2016-08-05%2012.05.59.jpg?dl=0)

![The River from a Ledge](https://www.dropbox.com/s/z555iy61xkjc2xe/2016-08-05%2012.27.06%20HDR.jpg?dl=0)
![Jen Looking Thoughtfully at the Bridge](https://www.dropbox.com/s/fl4mvvrnqu65ha0/2016-08-05%2011.56.05.jpg?dl=0)
![Jen ]

I view my love of woods as a complement to my interest in understanding how people and, in particular, creatives think. Writers, scientists, and artists throughout history have taken advantage of the awe-inducing and reality-shifting characteristics of nature to shake them out of their usual ways of thinking. While I can't claim to reside in the same league with the likes of [Jung, Wittgenstein, and Thoreau](https://paradelle.wordpress.com/2013/02/09/you-should-not-need-a-cabin-to-be-a-writer-but/), I like to think that the occasional nature hike strokes my creative juices as it did theirs.
![A Good Quote](dl.dropbox.com/s/xp36nuce2zekmi3/2016-08-05%2011.53.28.jpg?dl=0)

After the bridge, we spent time exploring two of Vancouver's neighborhoods, Gastown and Chinatown, both of which fell short of expectations. We did make it to the Sun Yat Sen communal garden in Chinatown, at which I took a few OK pictures.
![Entranceway to the Garden](https://www.dropbox.com/s/ir50uiuhci52z9i/2016-08-05%2012.30.52.jpg?dl=0)
![Lillipad Pond](https://www.dropbox.com/s/88gjkwl1bzgh24v/2016-08-05%2014.11.13.jpg?dl=0)

We ate dinner at Joe Fortes', a monument to seafood, where I surprisingly enjoyed our chatty waiter's banter.
![Sablefish with Kale and Quinoa](https://www.dropbox.com/s/88gjkwl1bzgh24v/2016-08-05%2014.11.13.jpg?dl=0)

After dinner, we made our way down to the water to see the temporarily lit Olympic torch.
![Olympic Torch](https://www.dropbox.com/s/olf2uth2rslsbwc/2016-08-05%2020.43.21.jpg?dl=0)

---
date: 2018-02-11
date: 2018-02-11
layout: post
title: JIT Reading Outline
category: posts
draft: true
---
# High-Level Outline
1. Focus on code comprehension rather than code reading. When we focus on code reading, we're
   prioritizing an image of what's virtuous over the actual goal -- to understand something enough
   to change it or evolve it in a different domain.
2. For comprehension, active beats passive, goal-directed beats random, and critical beats
   unquestioning. No one who cares about comprehension should encourage passive code reading.
3. How can we design codebases for active, goal-directed, and critical reading? Tests are good for
   active fiddling but don't do enough at a high-level to help goal-directed explorers. A
   goal-directed explorer cares about finding something similar to what they want to do and
   understanding the intent of what's there enough to know what they can throw away.

# Detailed Outline
1. Introduction: Comprehension as goal.
    1. Seibel should've asked people what code they learned about or hacked on for fun. What
       matters is that they learned about a codebase for it's own sake, not that they read it.
    2. Comprehension comes about from the interplay of active and passive exploration (Cal
       Newport's textbook method) - reading code to hack on it or understand how it accomplishes
       something specific. Group this under "purposeful reading".
2. Active over passive. Although very few people seem to read code passively, let's beat
   into the ground why it's inferior to purposeful reading.
    1. Learning research shows that active exploration beats passive consumption for comprehension
       in general (can cite [Cal
       Newport](http://calnewport.com/blog/2012/08/10/you-know-what-you-write-the-textbook-method-for-ultra-learning),
       Anders Ericsson, [Herbert
       Simon](http://158.132.155.107/posh97/private/learning/learning-Simon.htm), and [Barbara
       Oakley](http://nautil.us/issue/40/learning/how-i-rewired-my-brain-to-become-fluent-in-math-rp)).
    2. The best learning happens when you're actively interacting with the material
       through synthesis and reframing, critically examining its claims, and building fluency
       through active repetition .
    3. Let's see how passive code stacks up against hacking-driven exploration for comprehension:
        1. Passive reading is linear but random. It's hard for a passive reader to be critical
           because they lack clear measures (beyond local features like "clean") with which to
           examine the code and all features of the code seem equally important. 
        2. Purposeful reading is non-linear but goal-directed. A purposeful reader critically
           examines the code and weighs the relative importance of its features in order to decide
           whether it does what he or she needs.
        3. Neither involves much repetition but purposeful reading involves more remixing (not
           totally sure whether to include this?).
        4. Passive code readers may be aware of a larger area of the codebase, but lack
           contextualized knowledge.
    4. Can frame this comparison by comparing a codebase to a map (blurb copy-pasted).
        > A codebase resembles a map. You can ask an infinite number of questions of a map. How far
        > is it from A to B? Which is the nearest town to C? It makes no sense to read a map
        > linearly from top to bottom, left to right. Obviously trying to enumerate all possible
        > questions is an exercise in futility.
3. Designing for purposeful reading (and hacking). Not sure of the final answer, but here are some
   ideas.
    1. Tests are good.
    2. But tests don't give a higher-level view. Especially important for cross-cutting features.
        1. Can we write documentation that focuses on mapping edges rather than nodes? Less what
           this thing does and more how different ideas flow through the code. De-emphasize local
           messiness in exchange for keeping high-level map up-to-date?
    3. Project-specific katas build fluency through repetition.
4. Conclusion: Assume future readers will explore purposefully and hack.
    1. Trade time spent adding more information for time spent coming up with things a future
       programmer can do that will require them understanding the information you would've written
       down.
    2. Prioritize fixing or create high-level maps which emphasize historical intent over cleaning
       up local features.
